from rest_framework import viewsets
from .models import Profiles
from .serializer import ProfileSerializer

class ProfilesViewSet(viewsets.ModelViewSet):
    queryset = Profiles.objects.all()
    serializer_class = ProfileSerializer