from rest_framework import routers
from .viewsets import ProfilesViewSet

router = routers.SimpleRouter()
router.register('profiles', ProfilesViewSet)

urlpatterns = router.urls
